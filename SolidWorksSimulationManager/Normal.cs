﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Windows.Media.Media3D;

namespace SolidWorksSimulationManager
{
    public class Normal
    {

        public Point3D start;

        public Vector3D normal;

        public Normal(Point3D start, Vector3D normal) {

            this.normal = normal;
            this.start = start;

        }

    }
}
