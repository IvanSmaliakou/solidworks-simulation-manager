﻿namespace SolidWorksSimulationManager
{
    partial class ResearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResearchForm));
            this.ResearchListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DoResearchButton = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ShowResultsButton = new System.Windows.Forms.Button();
            this.AddResearchButton = new System.Windows.Forms.Button();
            this.DeleteResearchButton = new System.Windows.Forms.Button();
            this.LoadSWButton = new System.Windows.Forms.Button();
            this.IndicatorButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.stressStrainPicker = new System.Windows.Forms.ComboBox();
            this.AddCurrentResearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ResearchListBox
            // 
            this.ResearchListBox.FormattingEnabled = true;
            this.ResearchListBox.ItemHeight = 16;
            this.ResearchListBox.Location = new System.Drawing.Point(29, 90);
            this.ResearchListBox.MultiColumn = true;
            this.ResearchListBox.Name = "ResearchListBox";
            this.ResearchListBox.Size = new System.Drawing.Size(483, 132);
            this.ResearchListBox.TabIndex = 0;
            this.ResearchListBox.SelectedIndexChanged += new System.EventHandler(this.ResearchListBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(45, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(261, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Список исследований";
            // 
            // DoResearchButton
            // 
            this.DoResearchButton.Location = new System.Drawing.Point(518, 104);
            this.DoResearchButton.Name = "DoResearchButton";
            this.DoResearchButton.Size = new System.Drawing.Size(174, 43);
            this.DoResearchButton.TabIndex = 2;
            this.DoResearchButton.Text = "Провести исследование";
            this.DoResearchButton.UseVisualStyleBackColor = true;
            this.DoResearchButton.Click += new System.EventHandler(this.DoResearchButton_Click);
            // 
            // ShowResultsButton
            // 
            this.ShowResultsButton.Location = new System.Drawing.Point(518, 179);
            this.ShowResultsButton.Name = "ShowResultsButton";
            this.ShowResultsButton.Size = new System.Drawing.Size(174, 43);
            this.ShowResultsButton.TabIndex = 3;
            this.ShowResultsButton.Text = "Показать результат";
            this.ShowResultsButton.UseVisualStyleBackColor = true;
            this.ShowResultsButton.Click += new System.EventHandler(this.ShowResultsButton_Click);
            // 
            // AddResearchButton
            // 
            this.AddResearchButton.Location = new System.Drawing.Point(29, 229);
            this.AddResearchButton.Name = "AddResearchButton";
            this.AddResearchButton.Size = new System.Drawing.Size(111, 80);
            this.AddResearchButton.TabIndex = 4;
            this.AddResearchButton.Text = "Добавить исследование";
            this.AddResearchButton.UseVisualStyleBackColor = true;
            this.AddResearchButton.Click += new System.EventHandler(this.AddResearchButton_Click);
            // 
            // DeleteResearchButton
            // 
            this.DeleteResearchButton.Location = new System.Drawing.Point(397, 229);
            this.DeleteResearchButton.Name = "DeleteResearchButton";
            this.DeleteResearchButton.Size = new System.Drawing.Size(115, 80);
            this.DeleteResearchButton.TabIndex = 5;
            this.DeleteResearchButton.Text = "Удалить исследование";
            this.DeleteResearchButton.UseVisualStyleBackColor = true;
            this.DeleteResearchButton.Click += new System.EventHandler(this.DeleteResearchButton_Click);
            // 
            // LoadSWButton
            // 
            this.LoadSWButton.Location = new System.Drawing.Point(698, 23);
            this.LoadSWButton.Name = "LoadSWButton";
            this.LoadSWButton.Size = new System.Drawing.Size(174, 61);
            this.LoadSWButton.TabIndex = 6;
            this.LoadSWButton.Text = "Загрузить SolidWorks";
            this.LoadSWButton.UseVisualStyleBackColor = true;
            this.LoadSWButton.Click += new System.EventHandler(this.LoadSWButton_Click);
            // 
            // IndicatorButton
            // 
            this.IndicatorButton.BackColor = System.Drawing.Color.Red;
            this.IndicatorButton.Location = new System.Drawing.Point(841, 90);
            this.IndicatorButton.Name = "IndicatorButton";
            this.IndicatorButton.Size = new System.Drawing.Size(31, 29);
            this.IndicatorButton.TabIndex = 8;
            this.IndicatorButton.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(601, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 60);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // stressStrainPicker
            // 
            this.stressStrainPicker.AllowDrop = true;
            this.stressStrainPicker.FormattingEnabled = true;
            this.stressStrainPicker.Items.AddRange(new object[] {
            "Напряжение",
            "Деформация"});
            this.stressStrainPicker.Location = new System.Drawing.Point(698, 189);
            this.stressStrainPicker.Name = "stressStrainPicker";
            this.stressStrainPicker.Size = new System.Drawing.Size(179, 24);
            this.stressStrainPicker.TabIndex = 9;
            // 
            // AddCurrentResearch
            // 
            this.AddCurrentResearch.Location = new System.Drawing.Point(208, 229);
            this.AddCurrentResearch.Name = "AddCurrentResearch";
            this.AddCurrentResearch.Size = new System.Drawing.Size(111, 80);
            this.AddCurrentResearch.TabIndex = 10;
            this.AddCurrentResearch.Text = "Добавить текущее исследование";
            this.AddCurrentResearch.UseVisualStyleBackColor = true;
            this.AddCurrentResearch.Click += new System.EventHandler(this.AddCurrentResearch_Click);
            // 
            // ResearchForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 456);
            this.Controls.Add(this.AddCurrentResearch);
            this.Controls.Add(this.stressStrainPicker);
            this.Controls.Add(this.IndicatorButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LoadSWButton);
            this.Controls.Add(this.DeleteResearchButton);
            this.Controls.Add(this.AddResearchButton);
            this.Controls.Add(this.ShowResultsButton);
            this.Controls.Add(this.DoResearchButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ResearchListBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ResearchForm2";
            this.Text = "SolidWorks simulation";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ResearchListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DoResearchButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button ShowResultsButton;
        private System.Windows.Forms.Button AddResearchButton;
        private System.Windows.Forms.Button DeleteResearchButton;
        private System.Windows.Forms.Button LoadSWButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button IndicatorButton;
        private System.Windows.Forms.ComboBox stressStrainPicker;
        private System.Windows.Forms.Button AddCurrentResearch;
    }
}