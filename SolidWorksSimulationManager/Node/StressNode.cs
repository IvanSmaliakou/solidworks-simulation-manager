﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidWorksSimulationManager
{
    public class StressNode : IParameters
    {

        public readonly float Sx;
        public readonly float Sy;
        public readonly float Sz;
        public readonly float Txy;
        public readonly float Tyz;
        public readonly float Txz;
        public readonly float P1;
        public readonly float P2;
        public readonly float P3;
        public readonly float VON;
        public readonly float INT;

        private readonly Dictionary<string,float> param;

        public StressNode(
            float Sx,
            float Sy,
            float Sz,
            float Txy,
            float Tyz,
            float Txz,
            float P1,
            float P2,
            float P3,
            float VON,
            float INT) {

            this.Sx = Sx;
            this.Sy = Sy;
            this.Sz = Sz;
            this.Txy = Txy;
            this.Tyz = Tyz;
            this.Txz = Txz;
            this.P1 = P1;
            this.P2 = P2;
            this.P3 = P3;
            this.VON = VON;
            this.INT = INT;

            this.param = new Dictionary<string, float>();

            this.param.Add("Напряжение вдоль оси Ox", Sx);
            this.param.Add("Напряжение вдоль оси Oy", Sy);
            this.param.Add("Напряжение вдоль оси Oz", Sz);
            this.param.Add("Напряжение в направлении Y на YZ", Txy);
            this.param.Add("Напряжение в направлении Z на XZ", Tyz);
            this.param.Add("Напряжение в направлении Z на YZ", Txz);
            this.param.Add("Нормальное напряжение в первом главном направлении", P1);
            this.param.Add("Нормальное напряжение во втором главном направлении", P2);
            this.param.Add("Нормальное напряжение в третьем главном направлении", P3);
            this.param.Add("Напряжение Von Mises", VON);

        }

        public Dictionary<string, float> GetParameters() {
            return this.param;
        }

        public float GetParam(string param)
        {
            return this.param[param];
        }

    }
}
