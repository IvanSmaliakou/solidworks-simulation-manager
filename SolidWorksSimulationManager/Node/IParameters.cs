﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidWorksSimulationManager
{
    interface IParameters
    {
        Dictionary<string, float> GetParameters();

        float GetParam(string param);
    }
}
