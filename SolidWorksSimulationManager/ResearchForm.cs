﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

using CellsConstructor_v._1.Controllers;
using SolidWorks.Interop.sldworks;

namespace SolidWorksSimulationManager
{
    public partial class ResearchForm : Form
    {
        private readonly string currentResearchPlaceholder = "Активное исследование SW Simulation";

        SolidWorksLoader.SolidWorksLoaderForm loadForm = new SolidWorksLoader.SolidWorksLoaderForm();

        private SolidWorksFaceManager.FeatureFaceManager FFM;

        private SolidWorksFaceManager.FeatureFaceManager constFFM;

        private SolidWorksMaterialManager.MaterialManager MM;

        private SolidWorksSimulationManager.StudyManager SM;

        private SolidWorksLoader.SolidWorksApp sldApp;

        private CoordinateSystem coordinateSystem;

        private Dictionary<string, StaticStudyResults> resultsDict = new Dictionary<string, StaticStudyResults>();

        public ResearchForm()
        {
            InitializeComponent();
        }

        private void AddItem(ListBox listBox, object obj)
        {
            listBox.Items.Add(obj);
        }

        private void DelItem(ListBox listBox)
        {
            if (listBox.SelectedItem != null)
            {
                listBox.Items.Remove(listBox.SelectedItem);

            }
        }

        private void LoadSWButton_Click(object sender, EventArgs e)
        {
            IndicatorButton.BackColor = System.Drawing.Color.Yellow;
            loadForm.ShowDialog();

            sldApp = loadForm.sldApp;

            if (sldApp != null && sldApp?.swApp != null && sldApp?.swDoc != null)
            {

                ModelDoc2 swDoc = sldApp.swDoc as ModelDoc2;

                MathUtility swMathUtils = sldApp.swMathUtils as MathUtility;
                coordinateSystem = new CoordinateSystem(swMathUtils);

                FFM = new SolidWorksFaceManager.FeatureFaceManager(swDoc);

                constFFM = new SolidWorksFaceManager.FeatureFaceManager(swDoc);

                string[] paths = sldApp.GetPathsMaterialDataBase();
                MM = new SolidWorksMaterialManager.MaterialManager();
                foreach (string path in paths)
                {
                    MM.LoadDBMaterials(path);
                }

                SM = new SolidWorksSimulationManager.StudyManager(sldApp.COSMOSWORKS);
                IndicatorButton.BackColor = System.Drawing.Color.Green;

                MessageBox.Show("Приложение SolidWorks загружено", "SolidWorks loader",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void AddResearchButton_Click(object sender, EventArgs e)
        {
            if (sldApp != null && sldApp.swApp != null && sldApp.swDoc != null)
            {

                SolidWorksSimulationManager.SolidWorksStaticStudyCreate studyForm = new SolidWorksSimulationManager.SolidWorksStaticStudyCreate(FFM, MM);
                studyForm.ShowDialog();
                SolidWorksSimulationManager.StaticStudyRecord study = studyForm.record;

                if (study != null)
                {
                    AddItem(ResearchListBox, study);
                }
            }
            else
            {
                MessageBox.Show("Приложение SolidWorks не загружено!", "Ошибка проведения исследования!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DeleteResearchButton_Click(object sender, EventArgs e)
        {
            DelItem(ResearchListBox);
            this.resultsDict.Remove(ResearchListBox.GetItemText(ResearchListBox.SelectedItem));
        }

        private StaticStudyRecord GetSelectedStudyRecord()
        {
            return ResearchListBox.SelectedItem as StaticStudyRecord;
        }

        private void DoResearchButton_Click(object sender, EventArgs e)
        {
            StaticStudyResults results = null;

            try
            {
                try {
                    string currentItemText = ResearchListBox.GetItemText(ResearchListBox.SelectedItem);
                    if (currentItemText.Equals(this.currentResearchPlaceholder))
                    {
                        results = ResearchController.RunCurrentResearch(SM);
                    }
                    else
                    {
                        var studyRecord = GetSelectedStudyRecord();
                        ResearchController.ValidateResearch(sldApp, studyRecord);
                        results = ResearchController.RunResearch(SM, studyRecord);
                    }
                }
                
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Ошибка при запуске исследования SolidWorks Simulation",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                FFM = new SolidWorksFaceManager.FeatureFaceManager(sldApp.swDoc as ModelDoc2);

            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Ошибка проведения исследования!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (results != null) {
                object toInsert = null;
                int selIndex = ResearchListBox.SelectedIndex;
                switch (ResearchListBox.SelectedItem) {
                    case StaticStudyRecord ssr:
                        ssr.withResult = true;
                        toInsert = ssr;
                        break;
                    case string s:
                        toInsert = "✅ " + s;
                        break;
                }
                ResearchListBox.Items.RemoveAt(selIndex);
                ResearchListBox.Items.Insert(selIndex, toInsert);
                ResearchListBox.SelectedIndex = selIndex;
                ResearchListBox.SelectedItem = toInsert;
                string itemText = ResearchListBox.GetItemText(ResearchListBox.SelectedItem);
                if (!(resultsDict.TryGetValue(itemText, out StaticStudyResults _))){
                    resultsDict.Add(itemText, results);
                }
            }
            MessageBox.Show("Исследование завершено", "Исследование", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ResearchListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ShowResultsButton_Click(object sender, EventArgs e)
        {
            switch (stressStrainPicker.Text) {
                case "Напряжение":
                    StressNodeChartForm stress = new StressNodeChartForm(this.resultsDict[ResearchListBox.GetItemText(ResearchListBox.SelectedItem)].nodes);
                    stress.Show();
                    break;
                case "Деформация":
                    StrainNodeChartForm strain = new StrainNodeChartForm(this.resultsDict[ResearchListBox.GetItemText(ResearchListBox.SelectedItem)].nodes);
                    strain.Show();
                    break;
            }
        }

        private void AddCurrentResearch_Click(object sender, EventArgs e)
        {
            AddItem(ResearchListBox, this.currentResearchPlaceholder);
        }
    }
}
