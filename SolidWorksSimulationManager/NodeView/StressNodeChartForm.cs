﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Defaults;

namespace SolidWorksSimulationManager
{
    public partial class StressNodeChartForm : Form
    {

        private List<Node> nodes;

        private StressScrollLiveChart scrollChart;

        public StressNodeChartForm(IEnumerable<Node> nodes)
        {
            InitializeComponent();

            this.nodes = nodes.ToList();

            scrollChart = new StressScrollLiveChart(
                cartesianChartStress,
                hScrollBarStress,
                this.nodes,
                30);

            FillCheckBoxList(checkedListBoxParamStress, this.nodes[0].stress.GetParameters().Keys);

          
        }

        private void checkedListBoxParamStress_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;

            string param = clb.Items[e.Index].ToString();


            if (e.NewValue == CheckState.Checked)
            {
                scrollChart.ShowSeries(param);
            }
            else
            {
                scrollChart.HideSeries(param);
            }

        }

        private void FillCheckBoxList(CheckedListBox list, IEnumerable<string> names)
        {

            foreach (string name in names)
            {

                list.Items.Add(name, false);

            }

            list.SetItemChecked(0, true);
        }

        private void cartesianChartStress_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }

        private void StressNodeChartForm_Load(object sender, EventArgs e)
        {
            cartesianChartStress.AxisY.Add(new LiveCharts.Wpf.Axis
            {
                Title = "Напряжение, Н/м^2"
            });

            cartesianChartStress.AxisX.Add(new LiveCharts.Wpf.Axis
            {
                Title = "Узлы"
            });
        }
    }
}
